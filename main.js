import fetch from 'node-fetch';
import {response} from "express";
/**
* Задание 1 - Имитируем работу с сервером - Promise
*
* 1. Написать функцию getList, которая возвращает Promise с данными о списке задач, иммитируя
* задержку перед получением в 2 секунды:
    * [
* { id: 1, title: 'Task 1', isDone: false },
* { id: 2, title: 'Task 2', isDone: true },
* ]
* 2. Написать скрипт который получит данные из функции getList и выведет на экран список задач.
* 3. Изменить промис так, чтобы он возвращал ошибку
* 4. Дополнить скрипт так, чтобы если промис возвращает ошибку выводилось сообщение об ошибке
*/


const getList = () =>{
    return new Promise((res, rej) => {
        setTimeout(function (){
            if(Math.random() > 0.5) {
                res([{id: 1, title: 'Task 1', isDone: false},
                    {id: 2, title: 'Task 2', isDone: true}
            ])
        }
        else{rej('error')}},2000);
    })
}
const promise = getList()
promise.then(res => console.log(res))
    .catch(error => console.log(error))

/**
 * Задание 4 - Чейнинг (цепочки) промисов
 *
 * Написать функцию которая будет соберет строку "Я использую цепочки обещаний", конкотенируя каждое
 * слово через отдельный then блок с задержкой в 1 секунду на каждой итерации.
 * Результат вывести в консоль.
 */
function getString() {
    return new Promise(res => {
        // что происходит
        setTimeout( () => {
            res('Я')
        }, 1000)
    })
        .then(result => {
            return new Promise(res => {
                setTimeout(() => {
                    res(result + ' использую')
                }, 1000)
            })
        })
        .then(result => {
            return new Promise(res => {
                setTimeout( () => {
                    res(result + ' цепочки')
                }, 1000)
            })
        })
        .then(result => {
            return new Promise(res => {
                setTimeout( () => {
                    res(result + ' обещаний')
                }, 1000)
            })
        })
}

getString().then(result => { console.log(result) })

/*const getString = () => new Promise(res => setTimeout( () => res('Я'), 1000))
    .then(result => new Promise(res => setTimeout(() => res(result + ' использую'), 1000)))
    .then(result => new Promise(res => setTimeout( () => res(result + ' цепочки'), 1000)))
    .then(result => new Promise(res => setTimeout( () => res(result + ' обещаний'), 1000)))

getString().then(result => console.log(result));*/

/**
 * Задание 5 - Параллельные обещания
 *
 * Написать функцию которая будет соберет строку "Я использую вызов обещаний параллельно",
 * используя функцию Promise.all(). Укажите следующее время задержки для каждого
 * промиса возвращаего слова:
 * Я - 1000,
 * использую - 800
 * вызов - 1200
 * обещаний - 700
 * параллельно - 500
 * Результат вывести в консоль.
 */

const getNewString = () => {
    return Promise.all([
            new Promise(res => {
                setTimeout(() => res("Я"), 1000)
            }),
            new Promise(res => {
                setTimeout(() => res("использую"), 800)
            }),
            new Promise(res => {
                setTimeout(() => res("вызов"), 1200)
            }),
            new Promise(res => {
                setTimeout(() => res("обещаний"), 700)
            }),
            new Promise(res => {
                setTimeout(() => res("параллельно"), 500)
            })
        ]
    ).then(result => result.join(' '))
}

getNewString().then(result => console.log(result));

/**
 * Задание 6 - Напишите функцию, которая вернет название фильма в котором впервые встретилась
 * планета с именем planetName, используя предоставленный API (https://swapi.dev)
 * при отсутствии такого фильма выводить "Фильм не найден"
 * Варианты имен: Alderaan, Kamino
 */

async function getFilmName(planetName) {
    const data = await fetch('https://swapi.dev/api/films/').then(res => res.json());
    const films = data.results;
    for (let i = 0; i < films.length; i++) {
        const planets = await Promise.all(films[i].planets.map(planetUrl => fetch(planetUrl).then(res => res.json())));
        if (planets.find(planet => planet.name === planetName)) {
            return films[i].title
        }
    }
    return 'Фильм не найден'
}
getFilmName('Alderaan').then(res => console.log(res));

/**
 * Задание 7 - Напишите функцию delay(ms), которая возвращает промис,
 * переходящий в состояние "resolved" через ms миллисекунд.
 *
 * delay(2000).then(() => console.log('Это сообщение вывелось через 2 секунды'))
 */

async function delay(ms) {
    return new Promise(res => {
        setTimeout(() => res(ms), ms)
    })
}
delay(2000).then(result => console.log('Это сообщение вывелось через ' + result/1000 + ' секунды'));

/**
 * Задание 8 - Решите 5 задачу, используя, функцию delay
 */


const getFullString = () => {
    return Promise.all([
            new Promise(res => {
                delay(1000).then(() => res("Я"));
            }),
            new Promise(res => {
                delay(800).then(() => res("использую"));
            }),
            new Promise(res => {
                delay(1200).then(() => res("вызов"));
            }),
            new Promise(res => {
                delay(700).then(() => res("обещаний"));
            }),
            new Promise(res => {
                delay(500).then(() => res("параллельно"));
            })
        ]
    ).then(result => result.join(' '))
}

getFullString().then(result => console.log(result));

/**
 * Задание 9 - Напишите функцию, которая загрузит данные по первому фильму в котором встретилась планета Татуин, используя
 * предоставленный API (https://swapi.dev)
 */

async function getFilmInf(planetName) {
    const data = await fetch('https://swapi.dev/api/films/').then(res => res.json());
    const films = data.results;
    for (let i = 0; i < films.length; i++) {
        const planets = await Promise.all(films[i].planets.map(planetUrl => fetch(planetUrl).then(res => res.json())));
        if (planets.find(planet => planet.name === planetName)) {
            films[i].planets = planets;
            films[i] = await loadData(films[i])
            return films[i]
        }
    }
    return 'Not found';
}
async function loadData(film){
    for (const property in film) {
        const val = film[property];
        if (typeof val === 'string' && val.startsWith('https://')) {
            film[property] = fetch(val).then(res => res.json());
       }
        if (typeof val === 'object'){
            if (Array.isArray(val) && val.length > 0 && typeof val[0] === 'string' && val[0].startsWith('https://')){
                film[property] = await Promise.all(val.map(Url => fetch(Url).then(res => res.json())));
            }
        }
    }
    return film;
}
getFilmInf('Tatooine').then(res => console.log(res));

/**
 * Задание 10 - Напишите функцию, которая выведет название транспортного средства на котором впервые ехал Anakin Skywalker, используя
 * предоставленный API (https://swapi.dev)
 */

async function getVehicles(url, pilotName){
    const people = await fetch(url).then(res => res.json());
    const pilots = people.results;
    for (let i = 0; i < pilots.length; i++) {
        if (pilots[i].name === pilotName) {
            const vehicles = await Promise.all(pilots[i].vehicles.map(vehicleUrl => fetch(vehicleUrl).then(res => res.json())));
            return vehicles;
        }

    }
    if (typeof people.next === 'string' && people.next.startsWith('https://')){
       return  await getVehicles(people.next, pilotName);
    }
    return new Promise((resolve, reject) => {
        resolve("Not found__")

    })

    }

    console.log('***task10***')
   getVehicles("https://swapi.dev/api/people/", 'Anakin Skywalker').then(res => console.log(res))


/**
 * Задание 11 - Напишите эхо-сервер, который на запрос POST /echo будет возвращать тело запроса.
 * Напишите функцию которая посылает запрос на http://localhost:3000 POST /echo со следующей
 * полезной нагрузкой:
 * { message: "Привет сервис, я жду от тебя ответа"}
 * Примите ответ от сервера и выведите результат в консоль, используя синтаксис async/await.
 */

const data = {
    message: "Привет сервис, я жду от тебя ответа"
}
fetch('http://localhost:3000/echo/',{

    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
})
    .then(response => response.json()).then(result => console.log(result))
